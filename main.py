import curses

KEYS = ["q", "r", "p", "s"]
RPS = ["r", "p", "s"]
RPS_s = ["Rock", "Paper", "Scissors"]


def getInput(stdscr):
    key = None

    while key == None:
        try:
            key = stdscr.getkey()
            if not key in KEYS:
                key = None
        except:
            key = None
    
    return key

def decideWinner(a, b):
    a2 = RPS.index(a)
    b2 = RPS.index(b)

    #  r  <  p  <  s  <  r  < ...
    # 0%3 < 1%3 < 2%3 < 3%3 < ...

    if a2 == b2: # tie
        return -1
    elif a2 == (b2 - 1) % 3: # player 1 wins
        return 1
    else: # player 0 wins
        return 0

def gameWon(wins):
    if wins[0] >= 5:
        return 0
    elif wins[1] >= 5:
        return 1
    
    return -1

def main(stdscr):
    stdscr.timeout(500)

    player = 0
    keys = ["", ""]
    wins = [0, 0]

    while gameWon(wins) == -1:
        stdscr.addstr("\n" + ["First", "Second"][player] + " Player' turn [rps] >> ")

        key = getInput(stdscr)

        if key == "q":
            break

        keys[player] = key

        if player == 1:
            stdscr.clear()

            t0 = RPS_s[RPS.index(keys[0])]
            t1 = RPS_s[RPS.index(keys[1])]

            stdscr.addstr("\n" + t0 + " vs " + t1 + "\n")
            
            winner = decideWinner(keys[0], keys[1])

            if winner != -1:
                stdscr.addstr(
                    RPS_s[RPS.index(keys[winner])]
                    + " wins against "
                    + RPS_s[RPS.index(keys[(winner + 1) % 2])]
                    + "\n"
                )

                wins[winner] += 1
            else:
                stdscr.addstr("It's a tie!\n")
            
            stdscr.addstr(str(wins[0]) + " to " + str(wins[1]) + "\n")

            stdscr.refresh()

        player = (player + 1) % 2
    
    stdscr.clear()
    stdscr.addstr(["First", "Second"][gameWon(wins)] + " Player won!\n")
    stdscr.addstr("The final scores are " + str(wins[0]) + " to " + str(wins[1]) + "\n")

    stdscr.addstr("\nPress any key to exit. ")
    exitkey = None
    while exitkey == None:
        try:
            exitkey = stdscr.getkey()
        except:
            exitkey = None

curses.wrapper(main)
